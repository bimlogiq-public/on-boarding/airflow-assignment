# Background
Revit is a building information modeling (BIM) software developed by Autodesk, used by architects, engineers, and construction professionals. It allows users to design and document building structures in a 3D model-based environment.

HVAC (Heating, Ventilation, and Air Conditioning) systems are used to regulate temperature and air quality in buildings. In Revit, HVAC systems are modeled using various elements such as ducts, air terminals, and mechanical equipment.

This assignment focuses on creating a Revit command to calculate the total airflow of an HVAC system.
# System Airflow
Every HVAC system consists of multiple elements such as ducts, air terminals, mechanical equipment, etc. All the elements in the system are connected together to transfer air from point A to point B.

Each element has multiple connectors which allow air transfer from one element to another. All the elements in the system are connected together through their connectors.

For this assignment, your aim is to develop a Revit command which helps the user to calculate the entire HVAC system airflow. The user selects the first element of the system and the command should find all the connected Air Terminals and sum up the airflow value of each air terminal to get the total airflow in the system.

For example, the following system contains 2 air terminals:

![RTF](images/system.png)

Each air terminal in this system has an airflow value of 40.0 L/S. The total airflow of the system would be 40.0 + 40.0 = 80. In Revit, you can check the airflow value of each air terminal on the properties panel:

![RTF](images/terminal_airflow.png)

You can use the Revit file provided in 'Model/AirflowSystem.rvt' to explore a sample HVAC system and test your code.
In this file, the user selects the element marked with a red arrow, and the task is to calculate the total amount of airflow coming from the air terminals.![RTF](images/sample_file_image.png)


## Revit API
In this section, we review some of the Revit API classes and methods which you would need to know for implementing this assignment. you can also use the  online [Revit API documentation](https://www.revitapidocs.com/), which contains all the details about the Revit API.

You can use this Sandbox as the starting pont of the project:
https://gitlab.com/bimlogiq-public/on-boarding/revit-sandbox

### Get Element object
In a Revit document, all elements have their own unique id which known as `ElementId`. Using the `Document` object which is passed by revit API as one of the command arguments, you can get each element from model using [document.GetElement()](https://www.revitapidocs.com/2015/d9848d7d-5917-2433-8454-f65f5ac03964.htm) method:
```
public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
{
    var uiDocument = commandData.Application.ActiveUIDocument;
    var document = uiDocument.Document;

    var elementId = uiDocument.Selection.PickObject(ObjectType.Element, "Please select an element to start with");
    var selectedElement = document.GetElement(elementId);

    return Result.Succeeded;
}
```

The returned object is an instance of the [Element](https://www.revitapidocs.com/2015/eb16114f-69ea-f4de-0d0d-f7388b105a16.htm) class, which is a base class for most persistent data within a Revit document.

### Element Category
The [Element.Category](https://www.revitapidocs.com/2015/8990bd36-af08-fc99-496b-f94fcb056b21.htm) property represents the category or subcategory to which an Element belongs. It is used to identify the Element type. For example, anything in the duct Category is considered a duct. Other categories include air terminal and etc.
To find out the category associated with each element on the document, you can simply use the [Revit lookup](https://github.com/jeremytammik/RevitLookup) tool. Select the element and use the `Snoop Selection...` command to list the element properties:

![RTF](images/category.png)


### Connector Manager
As mentioned earlier, each element has several connectors. Each element connects to other elements, through its connectors. Both [FamilyInstance](https://www.revitapidocs.com/2018/0d2231f8-91e6-794f-92ae-16aad8014b27.htm) and [MEPCurve](https://www.revitapidocs.com/2015/38714847-0f40-7021-aa79-2884c3a02ce2.htm) which are Elements derivative classes, have the ConnectorManager property which allows you to get access to the list of elements [Connectors](https://www.revitapidocs.com/2017/11e07082-b3f2-26a1-de79-16535f44716c.htm):
```
public static ConnectorSet GetConnectors(Element element)
{
    if (element is FamilyInstance fi && fi.MEPModel != null)
    {
        return fi.MEPModel.ConnectorManager.Connectors;
    }
    else if (element is MEPCurve duct)
    {
        return duct.ConnectorManager.Connectors;
    }

    return null;
}
```

### Get Element Parameters
Each element contains a list of parameters that are shown in the properties panel in Revit when you select the element. To access these parameters programmatically you can use `element.get_Parameter()` method. For example, to get the air flow parameter form the air terminal element you can use the following method:
```
double airFlow = element.get_Parameter(Autodesk.Revit.DB.BuiltInParameter.RBS_DUCT_FLOW_PARAM)?.AsDouble() ?? 0;
```

### Note
Although the Revit API includes built-in concepts like [MechanicalSystem](https://www.revitapidocs.com/2023/557ae19f-3f61-945e-f3bd-be4d233f0d52.htm) and [DuctNetwork](https://www.revitapidocs.com/2023/93b81f99-e92e-d6ab-f459-eeb71717e809.htm) (which often have functional issues), we are asking you to developing your own algorithm for airflow calculation.